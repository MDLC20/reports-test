package com.qaservices.util;

public class DataExpect {
 
	public String codigo;
	public String name;
	public String descripcion;
	
	public DataExpect() {

	}
	
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public DataExpect(String codes, String filenames, String descriptions) {
		this.codigo=codes;
		this.name=filenames;
		this.descripcion=descriptions;
	}
	@Override
	public String toString() {
		return "DataExpect [codigo=" + codigo + ", name=" + name + ", descripcion=" + descripcion + "]";
	}
	
}
