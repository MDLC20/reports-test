package com.qaservices.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class conexionBD {

public static void main(String[] args) {
		
		
		
		String codigoExpect="";
		String namefileExpect="";
		String descriptionExpect="";
		
		String user="ebill";
		String password="ebill123";
		
		String driver ="com.microsoft.sqlserver.jdbc.SQLServerDriver";
	    String url ="jdbc:sqlserver://LAPTOP-TCGKRGGR:1433;databaseName=PRUEBAS";
	   
		try {
			Class.forName(driver);
			System.out.println("Exito al cargar el driver");
		}catch(Exception e){
			System.err.println("Error la cargar el driver "+ e.getMessage());
//			e.printStackTrace();
			return;
		}
		
		try {
			Connection cn = DriverManager.getConnection(url, user, password);
			System.out.println("Exito en conexion");
			
			String sql = "SELECT * FROM dataTest";
			Statement st = cn.createStatement();
			ResultSet rs = st.executeQuery(sql);
			
		   
		   List<DataExpect> expected = new ArrayList<DataExpect>();	
			
			while (rs.next()) {
				
				codigoExpect = rs.getString("codigo");
				namefileExpect = rs.getString("namefile");
				descriptionExpect = rs.getString("descripcion");
				DataExpect e = new DataExpect(codigoExpect,namefileExpect,descriptionExpect);
				expected.add(e);
				}
			
			System.out.println("Lista :" + expected);
	    
	/*		for(String a : expected) {
				
			}*/
			
		}catch (Exception e) {
			System.err.println("Error en conexion "+ e.getMessage());
//			e.printStackTrace();
		}
	}
	
public static String consultaCodigo(String filename) {
	
	String codigoExpect=null;
	
	String user="ebill";
	String password="ebill123";
	
	String driver ="com.microsoft.sqlserver.jdbc.SQLServerDriver";
    String url ="jdbc:sqlserver://LAPTOP-TCGKRGGR:1433;databaseName=PRUEBAS";
   
	try {
		Class.forName(driver);
		System.out.println("Exito al cargar el driver");
		Connection cn = DriverManager.getConnection(url, user, password);
		System.out.println("Exito en conexion");
		
		String sql = "SELECT TOP 1 * FROM dataTest WHERE namefile = ?";
		PreparedStatement ps = cn.prepareStatement(sql);
		ps.setString(1, filename);
		ResultSet rs = ps.executeQuery();
		
	   
	   List<DataExpect> expected = new ArrayList<DataExpect>();	
		if (rs.next()) {
			codigoExpect = rs.getString("codigo");
		}
		
		return codigoExpect;
		
	}catch(Exception e){
		System.err.println("Error la cargar el driver "+ e.getMessage());
		e.printStackTrace();
		return null;
	}
	
}

public static DataExpect obtenerDatos(String filename) {
	
//	String codigoExpect=null;
	
	DataExpect e = null;
	
	String user="ebill";
	String password="ebill123";
	
	String driver ="com.microsoft.sqlserver.jdbc.SQLServerDriver";
    String url ="jdbc:sqlserver://LAPTOP-TCGKRGGR:1433;databaseName=PRUEBAS";
   
	try {
		Class.forName(driver);
		System.out.println("Exito al cargar el driver");
		Connection cn = DriverManager.getConnection(url, user, password);
		System.out.println("Exito en conexion");
		
		String sql = "SELECT TOP 1 * FROM dataTest WHERE namefile = ?";
		PreparedStatement ps = cn.prepareStatement(sql);
		ps.setString(1, filename);
		ResultSet rs = ps.executeQuery();
		
	   
	  // List<DataExpect> expected = new ArrayList<DataExpect>();	
		if (rs.next()) {	
			e = new DataExpect();
			e.setCodigo( rs.getString("codigo"));
			e.setDescripcion(rs.getString("descripcion"));
		}
		
		return e;
		
	}catch(Exception m){
		System.err.println("Error la cargar el driver "+ m.getMessage());
		m.printStackTrace();
		return null;
	}
	
}
	
}
