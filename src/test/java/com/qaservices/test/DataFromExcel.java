package com.qaservices.test;

import org.testng.annotations.Test;

import com.qaservices.util.ExcelFile;

public class DataFromExcel {

	
	
	@Test
	public Object ObteniendoData() {
		
		String excelPath = "C:\\Users\\Michael\\Documents\\EclipseSoftw\\ReadDataTest\\readdata-demofromexcel\\data\\2020-10-22-12-38.xlsx";
		
		ExcelFile excel = new ExcelFile( excelPath, "HojaValidaciones");
		
		int rowCount = excel.getRowCount();
		int colCount = excel.getColCount();
		Object data[][] = new Object[rowCount-1][colCount];
		
		for (int i = 1; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				
				String CellData = excel.getCellDataString(i,j);
				System.out.println(CellData + " | ");
				data[i-1][j] = CellData;
				
			}
		}
		return data;
	}
	
	
}
