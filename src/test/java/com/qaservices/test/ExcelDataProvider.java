package com.qaservices.test;

import java.util.concurrent.ThreadLocalRandom;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.qaservices.util.DataExpect;
import com.qaservices.util.ExcelFile;
import com.qaservices.util.conexionBD;

public class ExcelDataProvider {

	int int_random = ThreadLocalRandom.current().nextInt(1,3);
	
	ExtentHtmlReporter report = new ExtentHtmlReporter("./report/Reporte"+int_random+".html");
	ExtentReports extent = new ExtentReports();
	
	@Test(dataProvider="DataTest")
	public void TestWithExcelData(String nameFile, String code, String message ) throws InterruptedException {
		
	    
		extent.attachReporter(report);
		ExtentTest logger = extent.createTest("Prueba errores - codigo " + code );
		
	
	    //	String codigoResult = conexionBD.consultaCodigo(nameFile);
		//	System.out.println(" "+ codigoResult);
		//	 if(codigoResult != null) {
		//	 if(codigoResult.equals(code)) {
		//		 System.out.println("PASSED");
		//	 } else {
		//		 System.out.println("FAILD");
		//	 }
		// }
		DataExpect codigoResult = conexionBD.obtenerDatos(nameFile);
		System.out.println(" "+ codigoResult);
		 if(codigoResult != null) {
			 if(codigoResult.getCodigo().equals(code) && codigoResult.getDescripcion().equals(message)) {
				 System.out.println("PASSED");
				 logger.log(Status.PASS, "Caso correcto")
				 .info("Codigo : "+ code)
				 .info("Descripcion : " + message);
			 } else {
			//	 logger.log(Status.FAIL, "Fallido").info("Codigo Actual: "+ code + " Codigo Esperado : "+ codigoResult.getCodigo()+ "Descripcion :" + message + " Descripcion Esperada : "+ codigoResult.getDescripcion());
				  if(!codigoResult.getCodigo().equals(code)) {
					  logger.log(Status.FAIL, "Error en codigo")
					  .info("Codigo Actual: "+ code)
					  .info("\n\nCodigo Esperado : "+ codigoResult.getCodigo());
				  }
				  if(!codigoResult.getDescripcion().equals(message)) {
					  logger.log(Status.FAIL, "Error en mensaje")
					  .info("Descripcion Actual :" + message)
					  .info("\n\n Descripcion Esperada : "+ codigoResult.getDescripcion());
				  }
			 }
			 extent.flush(); 
		 } 
		 
		
	}

/*	@Test(dataProvider="DataTest")
	public void TestWithExcelData(String nameFile, String code, String message ) throws InterruptedException {
		
	
		extent.attachReporter(report);
		ExtentTest logger = extent.createTest("Prueba errores - codigo " + code );
		ExtentTest childTest = logger.createNode("MyFirstChildTest");
	
	/*	String codigoResult = conexionBD.consultaCodigo(nameFile);
		System.out.println(" "+ codigoResult);
		 if(codigoResult != null) {
			 if(codigoResult.equals(code)) {
				 System.out.println("PASSED");
			 } else {
				 System.out.println("FAILD");
			 }
		 }
		DataExpect codigoResult = conexionBD.obtenerDatos(nameFile);
		System.out.println(" "+ codigoResult);
		 if(codigoResult != null) {
			 if(codigoResult.getCodigo().equals(code) && codigoResult.getDescripcion().equals(message)) {
				 System.out.println("PASSED");
				 logger.log(Status.PASS, "Caso correcto");
				 childTest.log(Status.PASS, "Caso correcto");
			//	 logger.info("Codigo : "+ code);
		 //	 logger.info("Descripcion :" + message);
			 } else {
			//	 logger.log(Status.FAIL, "Fallido").info("Codigo Actual: "+ code + " Codigo Esperado : "+ codigoResult.getCodigo()+ "Descripcion :" + message + " Descripcion Esperada : "+ codigoResult.getDescripcion());
				  if(!codigoResult.getCodigo().equals(code)) {
					 childTest.log(Status.FAIL, "Error en codigo").info("Codigo Actual: "+ code + "\n\nCodigo Esperado : "+ codigoResult.getCodigo());
					//  logger.log(Status.FAIL, "Error en codigo").info("Codigo Actual: "+ code + "\n\nCodigo Esperado : "+ codigoResult.getCodigo());
				  }
				  if(!codigoResult.getDescripcion().equals(message)) {
					  childTest.log(Status.FAIL, "Error en mensaje").info("Descripcion Actual :" + message + "\n\n Descripcion Esperada : "+ codigoResult.getDescripcion());
					  //logger.log(Status.FAIL, "Error en mensaje").info("Descripcion Actual :" + message + "\n\n Descripcion Esperada : "+ codigoResult.getDescripcion());
				  }
			 }
			 extent.flush(); 
		 } 
		 
		
	}
	*/
	@DataProvider(name="DataTest")
	public Object[][] getData() {
		
		String excelPath = "C:\\Users\\Michael\\Documents\\EclipseSoftw\\ReadDataTest\\readdata-demofromexcel\\data\\2020-10-22-12-38.xlsx";
		return testData(excelPath, "Hoja de validaciones");
		
//		Object data = (Object) testData(excelPath, "Hoja de validaciones");
//		return (Object[][]) data;
		
	}
	
	public Object[][] testData(String excelPath, String sheetName) {
		
		ExcelFile excel  = new ExcelFile(excelPath, sheetName);
		
		int rowCount = excel.getRowCount();
		int colCount = excel.getColCount();
		
		Object data[][] = new Object[rowCount-1][colCount];
		
		for (int i = 1; i < rowCount; i++) {
			for (int j = 0; j < colCount; j++) {
				
				String CellData = excel.getCellDataString(i,j);
				//System.out.println(CellData + " | ");
				data[i-1][j] = CellData;
				
			}
			//System.out.println();
		}
		return data;
	}

}
